const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")

// routes folder - is where all http method and endpoints are located

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(
        result => res.send(result));
});

router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(
        result => res.send(result));
});

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(
        result => res.send(result));  
});

//S38 Activity 
router.post("/details", (req, res) => {
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
})


module.exports = router;
